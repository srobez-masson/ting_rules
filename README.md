# Ting - rules system to build query

## Build a rule

    <?php
    
    namespace AppBundle\Domain\Feed\Rules;
    
    use AppBundle\Domain\Ting\Rule;
    use Aura\SqlQuery\Common\SelectInterface;
    
    class IsEnabled implements Rule
    {
        public function getRule()
        {
            return 'enabled = "true"';
        }
    
        public function getParameters()
        {
            return [];
        }
    
        public function applyRule(SelectInterface $queryBuilder, string $rule, array $parameters = [])
        {
            return $queryBuilder
                ->where($rule)
                ->bindValues($parameters);
        }
    }


## Use your rule

    <?php
    $rulesApplier = new RulesApplier($feedRepository);
    $rulesApplier->rules([
        new IsEnabled()
    ]);
    $feeds = $rulesApplier->apply();
