<?php
/***********************************************************************
 *
 * Ting Rules - rules system to build query
 * ==========================================
 *
 * Copyright (C) 2014 CCM Benchmark Group. (http://www.ccmbenchmark.com)
 *
 ***********************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 *
 **********************************************************************/

namespace CCMBenchmark\TingRules;

use AppBundle\Domain\MetadataProvider;
use Aura\SqlQuery\Common\Select;
use Aura\SqlQuery\Common\SelectInterface;
use CCMBenchmark\Ting\Exception;
use CCMBenchmark\Ting\Query\Query;
use CCMBenchmark\Ting\Query\QueryException;
use CCMBenchmark\Ting\Repository\CollectionInterface;
use CCMBenchmark\Ting\Repository\HydratorInterface;
use CCMBenchmark\Ting\Repository\HydratorSingleObject;
use CCMBenchmark\Ting\Repository\Metadata;
use CCMBenchmark\Ting\Repository\Repository;

/**
 * RulesApplier
 */
class RulesApplier
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var array
     */
    private $rules = [];

    /***
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $rules
     *
     * @return $this
     */
    public function rules(array $rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * @param SelectInterface $queryBuilder
     *
     * @return SelectInterface
     */
    private function applyRules(SelectInterface $queryBuilder)
    {
        foreach ($this->rules as $rule) {
            $queryBuilder = $rule->applyRule($queryBuilder, $rule->getRule(), $rule->getParameters());
        }

        return $queryBuilder;
    }

    /**
     * @param Metadata $metadata
     *
     * @return array
     */
    private function getColumns(Metadata $metadata)
    {
        $columns = [];
        foreach ($metadata->getFields() as $field) {
            $columns[] = $field['columnName'];
        }

        return $columns;
    }

    /**
     * @param SelectInterface $select
     *
     * @return Query
     */
    private function buildQueryFromSelect(SelectInterface $select)
    {
        return $this->repository
            ->getQuery($select->getStatement())
            ->setParams($select->getBindValues());

    }

    /**
     * @params HydratorInterface $hydrator
     *
     * @throws Exception
     * @throws QueryException
     *
     * @return CollectionInterface
     */
    public function apply(HydratorInterface $hydrator = null)
    {
        $metadata = $this->repository->getMetadata();

        if ($hydrator === null) {
            $hydrator = new HydratorSingleObject();
        }

        /** @var SelectInterface $select */
        $select = $this->repository->getQueryBuilder(Repository::QUERY_SELECT);
        $select->from($metadata->getTable());

        $select = $this->applyRules($select);

        if (($select instanceof Select) === false || $select->hasCols() === false) {
            $select->cols($this->getColumns($metadata));
        }

        return $this
            ->buildQueryFromSelect($select)
            ->query($this->repository->getCollection($hydrator));
    }
}
