<?php
/***********************************************************************
 *
 * Ting Rules - rules system to build query
 * ==========================================
 *
 * Copyright (C) 2014 CCM Benchmark Group. (http://www.ccmbenchmark.com)
 *
 ***********************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 *
 **********************************************************************/

namespace CCMBenchmark\TingRules\Rules;

use CCMBenchmark\TingRules\Rule;
use Aura\SqlQuery\Common\SelectInterface;

class OrX implements Rule
{
    private $rules = [];

    public function __construct(Rule $rule)
    {
        $this->rules[] = $rule;
    }

    public function or(Rule $orOtherRule)
    {
        $this->rules[] = $orOtherRule;

        return $this;
    }

    public function getRule(): string
    {
        $rules = array_map(
            function($rule) {
                /** @var Rule $rule */
                return $rule->getRule();
            },
            $this->rules
        );
        return '(' . implode(') OR (', $rules) . ')';
    }

    public function getParameters(): array
    {
        return [];
    }

    public function applyRule(SelectInterface $queryBuilder, string $rule, array $parameters = []): SelectInterface
    {
        return $this->rules[0]->applyRule($queryBuilder, $rule, $parameters);
    }
}
